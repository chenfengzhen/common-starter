package com.yishuifengxiao.common.autoconfigure;

import java.lang.reflect.Method;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.yishuifengxiao.common.autoconfigure.support.ValidateAutoConfiguration;
import com.yishuifengxiao.common.tool.encoder.Md5;
import com.yishuifengxiao.common.utils.SpringContext;

/**
 * 注入一些全局通用的配置
 * 
 * @author yishui
 * @date 2019年2月13日
 * @version 0.0.1
 */
@Configuration
@Import(ValidateAutoConfiguration.class)
public class CommonAutoConfiguration {

	/**
	 * 注入一个spring 上下文工具类
	 * 
	 * @param applicationContext
	 * @return
	 */
	@Bean
	public SpringContext springContext(ApplicationContext applicationContext) {
		SpringContext springContext = new SpringContext();
		springContext.setApplicationContext(applicationContext);
		return springContext;
	}

	/**
	 * 生成一个自定义spring cache 键生成器
	 * 
	 * @return
	 */
	@Bean("simpleKeyGenerator")
	@ConditionalOnMissingBean(name = { "simpleKeyGenerator" })
	public KeyGenerator simpleKeyGenerator() {
		return new KeyGenerator() {

			@Override
			public Object generate(Object target, Method method, Object... params) {
				StringBuilder prefix = new StringBuilder(target.getClass().getSimpleName()).append(":")
						.append(method.getName()).append(":");
				StringBuilder values = new StringBuilder("");
				if (null != params) {
					for (Object param : params) {
						if (null != param) {
							values.append(param.toString());
						}
					}
				}
				return prefix.append(Md5.md5Short(values.toString())).toString();
			}
		};
	}

}
