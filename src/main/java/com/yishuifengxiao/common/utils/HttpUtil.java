package com.yishuifengxiao.common.utils;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yishuifengxiao.common.tool.entity.Response;

/**
 * http 工具类
 * 
 * @author yishui
 * @date 2020年6月23日
 * @version 1.0.0
 */
public class HttpUtil {

	private final static Logger log = LoggerFactory.getLogger(HttpUtil.class);

	/**
	 * 向http响应流中推送数据并关闭响应流
	 * 
	 * @param resp
	 * @param result
	 */
	public synchronized static void out(HttpServletResponse response, Response<Object> result) {
		response.setStatus(HttpStatus.OK.value());
		// 允许跨域访问的域，可以是一个域的列表，也可以是通配符"*"
		response.setHeader("Access-Control-Allow-Origin", "*");
		// 允许使用的请求方法，以逗号隔开
		response.setHeader("Access-Control-Allow-Methods", "*");
		// 是否允许请求带有验证信息，
		response.setHeader("Access-Control-Allow-Credentials", "true");
		response.setContentType("application/json;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().write(new ObjectMapper().writeValueAsString(result));
			response.getWriter().flush();
			response.getWriter().close();
		} catch (Exception e) {
			log.info("返回响应数据时出现问题，出现问题的原因为 {}", e.getMessage());
		}
	}
}
