package com.yishuifengxiao.common.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 请求的上下文
 * 
 * @author yishui
 * @date 2020年6月17日
 * @version 1.0.0
 */
public class RequestContext implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7967527202884314519L;
	/**
	 * 请求的缓存的名字
	 */
	public static final String CACHE_KEY = "REQUEST_CONTEXT_CACHE_KEY";

	/**
	 * 请求的序列号
	 */
	private String requestId;
	/**
	 * 请求的类的名字
	 */
	private String className;
	/**
	 * 请求的方法的名字
	 */
	private String methodName;
	/**
	 * 请求的参数
	 */
	private Object[] args;
	/**
	 * 被请求的目标的完整的名字
	 */
	private String fullName;
	/**
	 * 请求时间
	 */
	private LocalDateTime time;

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public Object[] getArgs() {
		return args;
	}

	public void setArgs(Object[] args) {
		this.args = args;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public LocalDateTime getTime() {
		return time;
	}

	public void setTime(LocalDateTime time) {
		this.time = time;
	}

	public RequestContext(String requestId, String className, String methodName, Object[] args, String fullName,
			LocalDateTime time) {
		this.requestId = requestId;
		this.className = className;
		this.methodName = methodName;
		this.args = args;
		this.fullName = fullName;
		this.time = time;
	}

	public RequestContext() {

	}

}
